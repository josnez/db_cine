package graficos;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.sql.*;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

public class VistaPeliculasCartelera extends JFrame {

	private JButton btnAtras;
	private JButton btnReservar;
	JTable tablaPeliculas;
	private VistaInicial visIni;
	JPanel laminaSuperior;
	JLabel lblMnsTitulo;
	JPanel laminaInferior;
	JPanel laminaCentral;
	JPanel laminaWest;
	JPanel laminaEast;
	DefaultTableModel modeloTabla;
	String Enviar;
	private JToolBar toolBar;
	private JButton btnInstrucciones;

	public VistaPeliculasCartelera(VistaInicial visIni) {
		Enviar = "PC";
		Toolkit miPantalla = Toolkit.getDefaultToolkit();
		Dimension tamanioPantalla = miPantalla.getScreenSize();

		int alturaPantalla = tamanioPantalla.height;
		int anchoPantalla = tamanioPantalla.width;

		setSize((int) (anchoPantalla / 1.5), alturaPantalla / 2);
		setLocation((int) (anchoPantalla / 5), alturaPantalla / 4);
		setTitle("Peliculas en cartelera");

		// A�adir icono ----------------------------------------------------
		Image miIcono;

		try {
			miIcono = ImageIO.read(new File("src/imagenes/icono1.png"));
			setIconImage(miIcono);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// -----------------------------------------------------------------

		// Inicializar botones
		laminaSuperior = new JPanel(); // lamina norte
		lblMnsTitulo = new JLabel("Peliculas en cartelera");
		btnAtras = new JButton("Atras");
		btnAtras.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				getVisPelCar().setVisible(false);
				visIni.iniciar();
			}

		});

		btnReservar = new JButton("Reservar");
		btnReservar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				visIni.getVisEsSi().setPeliculaEscogida(tablaPeliculas.getSelectedRow() + 1);
				visIni.getVisFun().iniciar(tablaPeliculas.getSelectedRow() + 1);;
				getVisPelCar().setVisible(false);
			}
		});
		// ---------------------------------------------------------------
		laminaInferior = new JPanel(); // lamina de botones
		laminaCentral = new JPanel(); // lamina de la tabla de peliculas
		laminaCentral.setLayout(new GridLayout());

		this.visIni = visIni;
		modeloTabla = new DefaultTableModel();
		// tablaPeliculas = new JTable(datosFilas, tablaNombres);
		tablaPeliculas = new JTable();
		tablaPeliculas.setModel(modeloTabla);
		tablaPeliculas.setBackground(Color.gray);
		laminaCentral.add(new JScrollPane(tablaPeliculas));

		// Espaciados ---------------------------------------

		laminaWest = new JPanel();
		laminaEast = new JPanel();

		// ---------------------------------------------------------------
		laminaSuperior.add(lblMnsTitulo);
		toolBar = new JToolBar();
		laminaInferior.add(toolBar);
		
		btnInstrucciones = new JButton("Instrucciones");
		toolBar.add(btnInstrucciones);
		btnInstrucciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Instrucciones I = new Instrucciones(Enviar);
				I.setVisible(true);
				I.setResizable(false);
			}
		});
		laminaInferior.add(btnAtras, BorderLayout.WEST);
		laminaInferior.add(btnReservar, BorderLayout.EAST);
		add(laminaSuperior, BorderLayout.NORTH);
		add(laminaInferior, BorderLayout.SOUTH);
		add(laminaCentral, BorderLayout.CENTER);
		add(laminaWest, BorderLayout.WEST);
		add(laminaEast, BorderLayout.EAST);
	}

	public void iniciar() {
		
		modeloTabla.setColumnCount(0);
		modeloTabla.setRowCount(0);
		modeloTabla.addColumn("Titulo");
		modeloTabla.addColumn("Fecha estreno");
		modeloTabla.addColumn("Clasificacion");
		modeloTabla.addColumn("Genero");
		modeloTabla.addColumn("Duracion");
		modeloTabla.addColumn("Descripcion");
		modeloTabla.addColumn("Director");
		modeloTabla.addColumn("Idioma");

		// Ejecutar consulta de la tabla pelicula --------------------------------

		ResultSet rsRegistros = null;
		String consulta = "SELECT * FROM pelicula";
		Statement sentencia = null;
		try {
			sentencia = visIni.getConexion().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			rsRegistros = sentencia.executeQuery(consulta);
			while (rsRegistros.next()) {
				
				String[] fila = new String[8];
				fila[0] = rsRegistros.getString("titulo");
				fila[1] = rsRegistros.getString("fecha_estreno");
				if(rsRegistros.getString("Cla_id_Clasi").equals("1"))
					fila[2] = "18+";
				else if(rsRegistros.getString("Cla_id_Clasi").equals("2"))
					fila[2] = "15+";
				else if(rsRegistros.getString("Cla_id_Clasi").equals("3"))
					fila[2] = "12+";
				if(rsRegistros.getString("Gen_id_genero").equals("1"))
					fila[3] = "Accion";
				else if(rsRegistros.getString("Gen_id_genero").equals("2"))
					fila[3] = "Aventura";
				else if(rsRegistros.getString("Gen_id_genero").equals("3"))
					fila[3] = "Drama/Aventura";
				else if(rsRegistros.getString("Gen_id_genero").equals("4"))
					fila[3] = "Fantasia/Ciencia Ficcion";
				fila[4] = rsRegistros.getString("duracion");
				fila[5] = rsRegistros.getString("descripcion");
				fila[6] = rsRegistros.getString("director");
				if(rsRegistros.getString("Idi_id_idioma").equals("1"))
					fila[7] = "Castellano/Latino";
				
				modeloTabla.addRow(fila);
			}
			sentencia.close();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// -------------------------------------------------------------------------
		
		setVisible(true);
	}

	public VistaPeliculasCartelera getVisPelCar() {
		return this;
	}
}