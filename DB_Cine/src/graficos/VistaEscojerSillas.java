package graficos;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.imageio.ImageIO;
import javax.swing.*;

public class VistaEscojerSillas extends JFrame {

	private int peliculaEscogida;
	private int asiento;
	private VistaInicial visIni;
	private Object[][] panelSillas = new Object[8][12];
	JLabel lblMnsTitulo;
	JPanel laminaSuperior;
	JPanel laminaCentral;
	JPanel lamTitulo;
	JPanel lamCen;
	String Enviar;
	private JToolBar toolBar;
	private JButton btnInstrucciones;
	
	public VistaEscojerSillas(VistaInicial visIni) {
		Enviar = "ES";
		this.visIni = visIni;

		Toolkit miPantalla = Toolkit.getDefaultToolkit();
		Dimension tamanioPantalla = miPantalla.getScreenSize();

		int alturaPantalla = tamanioPantalla.height;
		int anchoPantalla = tamanioPantalla.width;

		setSize((int) (anchoPantalla / 1.2), (int) (alturaPantalla / 1.5));
		setLocation((int) (anchoPantalla / 12), alturaPantalla / 5);
		setTitle("Reservar boleta");

		// A�adir icono ----------------------------------------------------
		Image miIcono;

		try {
			miIcono = ImageIO.read(new File("src/imagenes/icono1.png"));
			setIconImage(miIcono);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// -----------------------------------------------------------------

		lblMnsTitulo = new JLabel();
		laminaSuperior = new JPanel();
		laminaCentral = new JPanel();
		laminaCentral.setLayout(new BorderLayout());
		lamCen = new JPanel();
		lamCen.setLayout(new GridLayout(8, 12, 30, 10));

		laminaSuperior.setLayout(new BorderLayout());

		lamTitulo = new JPanel();
		lamTitulo.add(lblMnsTitulo);
		laminaSuperior.add(lamTitulo, BorderLayout.NORTH);

		JLabel lblMnsInstruccion = new JLabel("Selecciona una silla disponible: ");

		laminaSuperior.add(lblMnsInstruccion, BorderLayout.CENTER);

		JPanel laminaSur = new JPanel(); // lamina botones
		JButton btnAtras = new JButton("Atras");
		btnAtras.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				getVisEsSi().setVisible(false);
				visIni.getVisFun().setVisible(true);
			}

		});
		
		JToolBar toolBar = new JToolBar();
		laminaSur.add(toolBar);
		
		JButton btnInstrucciones = new JButton("Instrucciones");
		btnInstrucciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
						Instrucciones I = new Instrucciones(Enviar);
						I.setVisible(true);
						I.setResizable(false);
			}
		});
		toolBar.add(btnInstrucciones);
		laminaSur.add(btnAtras);

		JPanel lamEsIz = new JPanel();// Espaciado izq de las sillas
		JPanel lamEsDer = new JPanel(); // Espaciado de la parte deracha de las sillas

		add(laminaSuperior, BorderLayout.NORTH);
		
		add(laminaSur, BorderLayout.SOUTH);
		add(lamEsIz, BorderLayout.WEST);
		add(lamEsDer, BorderLayout.EAST);
		add(laminaCentral, BorderLayout.CENTER);
	}

	public void iniciar(int id_funcion) {
		
		if(lamCen.getComponentCount()!=0) {
			lamCen.removeAll();
		}

		// Ejecutar consulta de la tabla pelicula --------------------------------

		ResultSet rsRegistro = null;
		String consulta = "SELECT titulo FROM pelicula WHERE id_pelicula = " + peliculaEscogida;
		Statement sentencia;
		try {
			sentencia = visIni.getConexion().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			rsRegistro = sentencia.executeQuery(consulta);
			while (rsRegistro.next()) {
				lblMnsTitulo
						.setText("Reservar boleta para:                          " + rsRegistro.getString("titulo"));
				lblMnsTitulo.repaint();
			}
			sentencia.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		// -------------------------------------------------------------------------
		// Ejecutar consulta de la tabla pelicula --------------------------------

		rsRegistro = null;
		consulta = "SELECT * FROM organizacion_asiento WHERE Fun_id_funcion = " + id_funcion+" ORDER BY asientos";
		sentencia = null;
		try {
			sentencia = visIni.getConexion().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			rsRegistro = sentencia.executeQuery(consulta);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// -------------------------------------------------------------------------
		asiento = 0;

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 12; j++) {
				try {
					while (rsRegistro.next()) {
						asiento++;
						panelSillas[i][j] = new EvoJButton(asiento);
						if (rsRegistro.getString("es_id_estado").equals("1")) {
							if (rsRegistro.getString("ti_id_tipo").equals("1")) {
								((JButton) panelSillas[i][j]).setBackground(Color.WHITE);
								((JButton) panelSillas[i][j]).addActionListener(new ActionListener() {

									@Override
									public void actionPerformed(ActionEvent arg0) {
										if (visIni.getId_usuario() == 0) {
											JOptionPane.showMessageDialog(getVisEsSi(),
													"Debes iniciar sesion para continuar ", "Usuario no registrado",
													JOptionPane.INFORMATION_MESSAGE);
										} else if (visIni.getId_usuario() != 0) {

											visIni.getVisPagBol().iniciar(visIni.getId_usuario(), 1,
													((EvoJButton) arg0.getSource()).getAsiento(), peliculaEscogida, id_funcion);
											getVisEsSi().setVisible(false);
										}
									}

								});
								lamCen.add((JButton) panelSillas[i][j]);
							} else if (rsRegistro.getString("ti_id_tipo").equals("2")) {
								((JButton) panelSillas[i][j]).setBackground(Color.WHITE);
								((JButton) panelSillas[i][j]).setText("P");
								((JButton) panelSillas[i][j]).addActionListener(new ActionListener() {

									@Override
									public void actionPerformed(ActionEvent arg0) {

										if (visIni.getId_usuario() == 0) {
											JOptionPane.showMessageDialog(getVisEsSi(),
													"Debes iniciar sesion para continuar ", "Usuario no registrado",
													JOptionPane.INFORMATION_MESSAGE);
										} else if (visIni.getId_usuario() != 0) {
											visIni.getVisPagBol().iniciar(visIni.getId_usuario(), 1,
													((EvoJButton) arg0.getSource()).getAsiento(), peliculaEscogida, id_funcion);
											getVisEsSi().setVisible(false);
										}
									}

								});
								lamCen.add((JButton) panelSillas[i][j]);
							}

						} else if (rsRegistro.getString("es_id_estado").equals("2")) {
							((JButton) panelSillas[i][j]).setBackground(Color.RED);
							((JButton) panelSillas[i][j]).addActionListener(new ActionListener() {

								@Override
								public void actionPerformed(ActionEvent arg0) {

									if (visIni.getId_usuario() == 0) {
										JOptionPane.showMessageDialog(getVisEsSi(),
												"Debes iniciar sesion para continuar ", "Usuario no registrado",
												JOptionPane.INFORMATION_MESSAGE);
									} else if (visIni.getId_usuario() != 0) {
										JOptionPane.showMessageDialog(getVisEsSi(),
												"Silla reservada, escoje otra silla ", "Upps",
												JOptionPane.INFORMATION_MESSAGE);
									}
								}

							});
							lamCen.add((JButton) panelSillas[i][j]);
						}
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		lamCen.repaint();
		laminaCentral.add(lamCen, BorderLayout.CENTER);
		setVisible(true);
	}

	public VistaEscojerSillas getVisEsSi() {
		return this;
	}

	public void setPeliculaEscogida(int peliculaEscogida) {
		this.peliculaEscogida = peliculaEscogida;
	}

	public int getPeliculaEscogida() {
		return peliculaEscogida;
	}

}

class EvoJButton extends JButton {

	private int asiento;

	public EvoJButton(int asiento) {

		this.asiento = asiento;
	}

	public int getAsiento() {
		return asiento;
	}

}
