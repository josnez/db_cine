package graficos;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.imageio.ImageIO;
import javax.swing.*;

public class VistaPagarBoletos extends JFrame {

	private VistaInicial visIni;
	private JButton btnPagar;
	private int id_organizacion;
	private int asiento;
	private int sala;
	private int id_funcion;
	private int id_usuario;

	JPanel laminaSuperior;
	JPanel lamIzq;
	JLabel lblMnsPelicula;
	JPanel lamDerIn;
	JLabel lblSala;
	JLabel lblAsiento;
	JLabel lblTipUsu;
	JLabel lblValor;
	JCheckBox chkTargetaDebito;
	JCheckBox chkTargetaCredito;
	String Enviar;
	private JToolBar toolBar;
	private JButton btnInstrucciones;
	
	public VistaPagarBoletos(VistaInicial visIni) {
		Enviar = "PB";
		Toolkit miPantalla = Toolkit.getDefaultToolkit();
		Dimension tamanioPantalla = miPantalla.getScreenSize();

		int alturaPantalla = tamanioPantalla.height;
		int anchoPantalla = tamanioPantalla.width;

		setSize((int) (anchoPantalla / 2), alturaPantalla / 2);
		setLocation((int) (anchoPantalla / 4), alturaPantalla / 4);
		setTitle("Pagar boleta");
		setLayout(new BorderLayout());

		// A�adir icono ----------------------------------------------------
		Image miIcono;

		try {
			miIcono = ImageIO.read(new File("src/imagenes/icono1.png"));
			setIconImage(miIcono);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// -----------------------------------------------------------------

		this.visIni = visIni;
		
		laminaSuperior = new JPanel();
		laminaSuperior.setLayout(new BorderLayout());
		JLabel lblMnsTitulo = new JLabel("Paga tus entradas");
		JPanel lamTitulo = new JPanel();
		lamTitulo.add(lblMnsTitulo);
		laminaSuperior.add(lamTitulo, BorderLayout.NORTH);

		JLabel lblMnsInst = new JLabel("Selecciona un metodo de pago:");
		JPanel lamMnsInst = new JPanel();
		lamMnsInst.add(lblMnsInst);
		laminaSuperior.add(lamMnsInst, BorderLayout.CENTER);
		lamIzq = new JPanel();
		lamIzq.setLayout(new BorderLayout());
		lblMnsPelicula = new JLabel();
		lamIzq.add(lblMnsPelicula, BorderLayout.NORTH);
		
		lamDerIn = new JPanel();
		lamDerIn.setLayout(new GridLayout(4, 2));
		lamDerIn.setBackground(Color.GRAY);

		JLabel lblMnsSala = new JLabel("Sala: ");
		lamDerIn.add(lblMnsSala);
		
		lblSala = new JLabel();
		lamDerIn.add(lblSala);
		
		JLabel lblMnsAsiento = new JLabel("Asiento: ");
		lamDerIn.add(lblMnsAsiento);
		lblAsiento = new JLabel();
		lamDerIn.add(lblAsiento);
		JLabel lblMnsTipUsu = new JLabel("Tipo usuario: ");
		lamDerIn.add(lblMnsTipUsu);
		lblTipUsu = new JLabel();
		lamDerIn.add(lblTipUsu);
		JLabel lblMnsValor = new JLabel("Valor: ");
		lamDerIn.add(lblMnsValor);
		lblValor = new JLabel();
		lamDerIn.add(lblValor);
		lamIzq.add(lamDerIn, BorderLayout.CENTER);
		
		JPanel laminaCentral = new JPanel();
		laminaCentral.setLayout(new BorderLayout());
		JLabel lblMnsNumero = new JLabel("Numero de la tarjeta: ");
		chkTargetaDebito = new JCheckBox("Tarjeta Debito");
		JTextField txtNumero = new JTextField(15);
		
		chkTargetaDebito.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				btnPagar.setEnabled(true);	
				
			}
		});
		ButtonGroup grupoChecks = new ButtonGroup();
		JPanel lamChecks = new JPanel();

		chkTargetaCredito = new JCheckBox("Targeta Credito");
		chkTargetaCredito.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				btnPagar.setEnabled(true);			}
		});
		grupoChecks.add(chkTargetaDebito);
		grupoChecks.add(chkTargetaCredito);
		lamChecks.add(chkTargetaDebito);
		lamChecks.add(chkTargetaCredito);

		laminaCentral.add(lamChecks, BorderLayout.SOUTH);
		JPanel lamTxtNum = new JPanel();
		lamTxtNum.setLayout(new FlowLayout());
		lamTxtNum.add(txtNumero);
		
		laminaCentral.add(lblMnsNumero, BorderLayout.NORTH);
		laminaCentral.add(lamTxtNum, BorderLayout.CENTER);
		JToolBar toolBar = new JToolBar();
		lamChecks.add(toolBar);
		
		JButton btnInstrucciones = new JButton("Instrucciones");
		toolBar.add(btnInstrucciones);
		btnInstrucciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Instrucciones I = new Instrucciones(Enviar);
				I.setVisible(true);
				I.setResizable(false);
			}
		});
		JPanel laminaDer = new JPanel();
		btnPagar = new JButton("Pagar");
		btnPagar.setEnabled(false);
		btnPagar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				
				
				// Ejecutar consulta de la tabla Pelicula --------------------------------

				ResultSet rsRegistro;
				String consulta = "SELECT id_org FROM organizacion_asiento WHERE Fun_id_funcion = "+getId_funcion() +" AND asientos = "+getAsiento();
				Statement sentencia;
				try {
					sentencia = visIni.getConexion().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
							ResultSet.CONCUR_READ_ONLY);
					rsRegistro = sentencia.executeQuery(consulta);
					while (rsRegistro.next()) {

						setId_organizacion(Integer.parseInt(rsRegistro.getString("id_org")));
					}
					sentencia.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}

				// -------------------------------------------------------------------------
				
				// Ejecutar update de la organizacion usuario --------------------------------

				consulta = "UPDATE Organizacion_asiento SET Es_id_estado = 2 WHERE id_org="+getId_organizacion();
				sentencia = null;
				try {
					sentencia = visIni.getConexion().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
							ResultSet.CONCUR_READ_ONLY);
		            
		            sentencia.executeUpdate(consulta);
		            System.out.println("Update exitoso");
					sentencia.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}

				// -------------------------------------------------------------------------
				// Ejecutar insert en la tabla compra  --------------------------------

				consulta = "INSERT INTO Compra (Org_id_org, Usu_id_usu) VALUES ("+getId_organizacion()+", "+getId_usuario()+")";
				sentencia = null;
				try {
					sentencia = visIni.getConexion().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
							ResultSet.CONCUR_READ_ONLY);
		            
		            sentencia.executeUpdate(consulta);
		            System.out.println("Insert exitoso");
					sentencia.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}

				// -------------------------------------------------------------------------
				
				JOptionPane.showMessageDialog(getVisPaBol(),
						"Compra realizada exitosamente, sigue comprando ", "Felicidades",
						JOptionPane.INFORMATION_MESSAGE);
				visIni.getVisPelCar().setVisible(true);
				getVisPaBol().setVisible(false);
				
			}

		});
		
		laminaDer.setLayout(new FlowLayout());
		laminaDer.add(btnPagar);

		add(laminaSuperior, BorderLayout.NORTH);
		add(lamIzq, BorderLayout.WEST);
		add(laminaCentral, BorderLayout.CENTER);
		add(laminaDer, BorderLayout.EAST);
	}

	public void iniciar(int id_usuario, int sala, int asiento, int pelicula, int id_funcion) {

		setAsiento(asiento);
		setSala(sala);
		setId_funcion(id_funcion);
		setId_usuario(id_usuario);
		// Ejecutar consulta de la tabla Pelicula --------------------------------

		ResultSet rsRegistro;
		String consulta = "SELECT titulo FROM pelicula WHERE id_pelicula = " + pelicula;
		Statement sentencia;
		try {
			sentencia = visIni.getConexion().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			rsRegistro = sentencia.executeQuery(consulta);
			while (rsRegistro.next()) {

				lblMnsPelicula.setText(rsRegistro.getString("titulo"));
			}
			sentencia.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		// -------------------------------------------------------------------------

		

		// Ejecutar consulta de la tabla Sala --------------------------------

		rsRegistro = null;
		consulta = "SELECT nombre FROM sala WHERE id_sala = " + sala;
		sentencia = null;
		try {
			sentencia = visIni.getConexion().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			rsRegistro = sentencia.executeQuery(consulta);
			while (rsRegistro.next()) {

				lblSala.setText(rsRegistro.getString("nombre"));
				
			}
			sentencia.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		// -------------------------------------------------------------------------
		lblAsiento.setText(String.valueOf(asiento));

		// Ejecutar consulta de la tabla usuario --------------------------------

		rsRegistro = null;
		consulta = "SELECT tipo_usu FROM Tipo_Usuario t, usuario u "
				+ "WHERE t.id_tipo_usu= u.ti_id_tipo_usu AND id_usuario= " + id_usuario;
		sentencia = null;
		String tipoUsuario = null;
		try {
			sentencia = visIni.getConexion().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			rsRegistro = sentencia.executeQuery(consulta);
			while (rsRegistro.next()) {
				tipoUsuario = rsRegistro.getString("tipo_usu");
				lblTipUsu.setText(tipoUsuario);
			}
			sentencia.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		// -------------------------------------------------------------------------

		lblValor.setText(String.valueOf(calcularCosto(asiento, tipoUsuario)));
		setVisible(true);
	}

	private int calcularCosto(int asiento, String tipoUsuario) {
		int valor = 0;
		if (asiento > 24) {
			if (tipoUsuario.equals("Normal"))
				valor = 5000;
			else if (tipoUsuario.equals("Platino"))
				valor = 4000;
			else if (tipoUsuario.equals("Pluss"))
				valor = 3000;
		} else {
			if (tipoUsuario.equals("Normal"))
				valor = 15000;
			else if (tipoUsuario.equals("Platino"))
				valor = 10000;
			else if (tipoUsuario.equals("Pluss"))
				valor = 8000;
		}
		return valor;
	}

	public int getId_organizacion() {
		return id_organizacion;
	}
	
	public int getId_funcion() {
		return id_funcion;
	}

	public void setId_funcion(int id_funcion) {
		this.id_funcion = id_funcion;
	}

	public void setId_organizacion(int id_organizacion) {
		this.id_organizacion = id_organizacion;
	}
	public int getAsiento() {
		return asiento;
	}

	public int getSala() {
		return sala;
	}

	public void setAsiento(int asiento) {
		this.asiento = asiento;
	}

	public int getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}

	public void setSala(int sala) {
		this.sala = sala;
	}
	
	public VistaPagarBoletos getVisPaBol() {
		return this;
	}
}
