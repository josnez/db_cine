package logica;

import java.sql.*;

public class ConexionDB {

	private Connection connection = null;
	public Connection conectar() {
		
		try {
            // We register the PostgreSQL driver
            // Registramos el driver de PostgresSQL
            try { 
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException ex) {
                System.out.println("Error al registrar el driver de PostgreSQL: " + ex);
            }
            // Database connect
            // Conectamos con la base de datos
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/postgres",
                    "postgres", "postgres2019");

            boolean valid = connection.isValid(50000);
            System.out.println(valid ? "DB CONECTED" : "DB FAILED COMO EL SEMESTRE");
        } catch (java.sql.SQLException sqle) {
            System.out.println("Error: " + sqle);
        }
		return connection;
	}
}
