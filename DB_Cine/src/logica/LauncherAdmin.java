package logica;

import graficos.VistaAdmin;
import graficos.VistaInicial;

public class LauncherAdmin {

	public static void main(String[] args) {
		
		ConexionDB conexion = new ConexionDB();
		VistaAdmin vista = new VistaAdmin(conexion.conectar());
		vista.iniciar();
	}

}
