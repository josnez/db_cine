package logica;

import javax.swing.*;
import graficos.*;


public class Launcher {

	public static void main(String[] args) {

		ConexionDB conexion = new ConexionDB();
		VistaInicial visIni = new VistaInicial(conexion.conectar());
		visIni.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		visIni.iniciar();
		
		
		
	}

}
